__author__ = "Michal Trendak & Karolina Kondzielnik"

import math
import statistics as stat

import numpy as np

""""Klasa reprezentujaca obiekt binarnego drzewa decyzyjnego."""


class DecisionTree:
    def __init__(self, values, classes, num_attributes, attr_indexes, indexes, name="GLOWA   "):
        """Ramka danych przykladow uczących"""
        self.values = values
        """Ramka danych niosaca informacje o przypisaniach przykladow do klas"""
        self.classes = classes
        """Liczba atrybutow wykorzystywanych do budowy drzewa"""
        self.num_attributes = num_attributes
        """Lista indeksow atrybutow wykorzystywanych do budowy drzewa"""
        self.attr_indexes = attr_indexes
        """Lista indeksow przykladow wykorzystywanych do budowy drzewa"""
        self.indexes = indexes
        """Nazwa drzewa
        
        Wykorzystywane przy wyswietlaniu zbudowanego drzewa
        """
        self.name = name

        """Indeks atrybutu dzielacego ramke danych w danym punkcie drzewa"""
        self.attr_idx = None
        """Indeks przykladu, przed ktorym nalezy wykonac podzial ramki danych"""
        self.second_group_idx = None
        """Zdobycz informacyjna (Information Gain) dla atrybutu dzielacego (attr_idx)
        
        Poczatkowo przypisana zostaje wartosc minus nieskonczonosc, poniewaz wartosc ta bedziemy maksymalizowac.
        """
        self.ig = -float('inf')

        """Klasa w danym punkcie drzewa na podstawie klas przykladow w nim obecnych
        
        Wyznaczana jest najczesciej wystepujaca klasa wsrod przykladow
        """
        self.node_class = stat.mode(sum(classes.values[indexes].tolist(), []))
        """Wskazanie na lewe poddrzewo"""
        self.lhs = None
        """Wskazanie na prawe poddrzewo"""
        self.rhs = None

        if any(self.attr_indexes) or (  # czy pozostały jeszcze jakies atrybuty, ktorymi mozna podzielic
                self.attr_indexes.tolist() == [0]):  # metoda any() w przypadku [0] zwraca false, stad dodatkowy warunek
            self.choose_attribute_to_split()

    """Glowna metoda odpowiedzialna za wybor atrybutu na podstawie ktorego ma zostac wykonany podzial do poddrzew.
    
    Najpierw sprawdzane sa warunki na obecnosc w lisciu drzewa.
    Nastepnie obliczana zostaje zdobycz informacyjna kolejno dla wszystkich atrybutow, wynikiem jest przypisanie 
    do pol obiektu odpowiednich wartosci mowiacych o wybranym atrybucie.
    Kolejnym sprawdzanym warunkiem jest to, czy wybrany atrybut umozliwia podzial (dane maja rozroznialne wartosci, 
    sytuacja ta najczesciej wystepuje, gdy pozostaje tylko jeden atrybut).
    Wynikiem dzialania metody jest rozdzielenie w danym punkcie na dwa poddrzewa.
    """

    def choose_attribute_to_split(self):
        # sprawdzenie czy nie jestesmy juz w lisciu
        if self.is_leaf:
            return
        for i in self.attr_indexes:
            self.count_IG_for_attribute(i)
        # czy dla danego atrybutu sa rozroznialne wartosci
        if self.ig == -float('inf'):
            return
        x = self.values.values[self.indexes, self.attr_idx]
        lhs = np.nonzero(x > 0)[0]
        rhs = np.nonzero(x < 1)[0]

        # usuniecie wykorzystanego atrybutu
        new_idxs = np.array([n for n in self.attr_indexes if n != self.attr_idx])
        self.lhs = DecisionTree(self.values, self.classes, self.num_attributes, new_idxs,
                                np.array(self.indexes)[lhs], "LEWY    ")
        self.rhs = DecisionTree(self.values, self.classes, self.num_attributes, new_idxs,
                                np.array(self.indexes)[rhs], "PRAWY   ")

    """Metoda sprawdzajaca, czy rozpatrywane przyklady w danym miejscu drzewa tworza lisc."""

    @property
    def is_leaf(self):
        # klasy wynikowe dla wszystkich rozpatrywannych przykladow sa takie same
        are_all_same = all(elem == self.classes.values[self.indexes].tolist()[0] for elem in
                           self.classes.values[
                               self.indexes].tolist())
        # pozostal tylko jeden przyklad wiec nie da sie podzielic
        is_last = (len(self.indexes) == 1)
        return are_all_same or is_last

    """Metoda umozliwiajaca obliczenie zdobyczy informacyjnej dla atrybutu o danym indeksie oraz
    ustalenie czy aktualnie rozpatrywany atrybut jest lepszy od wczesniejszych."""

    def count_IG_for_attribute(self, attr_index):
        new_values = self.values.values[self.indexes, attr_index]
        new_classes = self.classes.values[self.indexes]
        new_sorted_values = new_values[np.argsort(new_values)]
        new_sorted_classes = new_classes[np.argsort(new_values)]

        if all(elem == new_values.tolist()[0] for elem in
               new_values.tolist()): return  # jesli dla atrybutu mamy tylko takie same wartosci to nie dokonamy podzialu

        IG_components = []
        IG_sizes = []

        # na pierwszym miejscu dodajemy ile T(1) i ile F(0) jest w kolumnie danego atrybutu
        IG_components.append([new_sorted_values.sum(), len(self.classes) - new_sorted_values.sum()])
        # do drugiej listy dodajemy licznosc zbioru
        IG_sizes.append(len(self.classes))
        # nastepnie sume wystepowania T
        IG_sizes.append(new_sorted_values.sum())
        # i sume wystepowania F
        IG_sizes.append(len(self.classes) - new_sorted_values.sum())
        counter_true = []
        counter_false = []
        second_group_idx = None

        tt = 0
        tf = 0
        ff = 0
        ft = 0
        # zliczanie odpowiednich par wartosci atrybutu i klasy
        for i in range(len(new_classes)):
            if new_sorted_values[i] == 1:
                if new_sorted_classes[i] == 1:
                    tt = tt + 1
                elif new_sorted_classes[i] == 0:
                    tf = tf + 1
            elif new_sorted_values[i] == 0:
                if new_sorted_classes[i] == 1:
                    ft = ft + 1
                elif new_sorted_classes[i] == 0:
                    ff = ff + 1
                second_group_idx = i + 1  # po przejsciu przez ostatnie zero otrzymamy indeks pierwszego elementu 1
            else:
                print("Else")
        counter_true.append(tt)
        counter_true.append(tf)
        counter_false.append(ft)
        counter_false.append(ff)
        IG_components.append(counter_true)
        IG_components.append(counter_false)

        ig = self.count_IG(IG_components, IG_sizes)
        if ig > self.ig:
            # jesli nowy atrybut lepszy, zapisujemy jego indeks, wartosc zdobyczy informacyjnej
            # oraz miejsce podzialu danych
            self.attr_idx, self.ig, self.second_group_idx = attr_index, ig, second_group_idx

    """Metoda umozliwiajaca obliczenie entropii dla podanych elementow skladowych."""

    def count_entropy(self, values, set_size):
        entropy = 0
        for i in range(values.__len__()):
            entropy = entropy + self.count_element(values[i], set_size)
        return entropy

    """Metoda umozliwiajaca obliczenie jednego skladnika sumy wykorzystywanej w obliczaniu entropii."""

    def count_element(self, first, second):
        if first == 0 or second == 0: return 0
        return -(first / second) * math.log2(first / second)

    """Metoda umozliwiajaca obliczenie zdobyczy informacyjnej."""

    def count_IG(self, values, sets_sizes):
        # pierwszy wiersz przechowuje informacje do obliczenia entropii atrybutu
        ig = self.count_entropy(values[0][:], sets_sizes[0])
        new_values = []
        for i in range(values[:][0].__len__()):
            # kolejne wiersze dla roznych wartosci rozpatrywanego atrybutu
            new_values.append(self.count_entropy(values[i + 1][:], sets_sizes[i + 1]))
        for j in range(new_values.__len__()):
            ig = ig - (values[0][j] / sets_sizes[0]) * new_values[j]
        return ig

    """Metoda umozliwiajaca wykonanie klasyfikacji nowego przykladu."""

    def classify(self, example):
        if self.is_tree_leaf:  # jesli lisc drzewa, zwroc klase
            return self.node_class
        a = example[self.attr_idx]
        # w zaleznosci od wartosci atrybutu dla przykladu przejdz lewa lub prawa krawedzia
        t = self.lhs if a == 1 else self.rhs
        return t.classify(example)

    """Metoda umozliwiajaca sprawdzenie czy dany obiekt jest listem drzewa."""

    @property
    def is_tree_leaf(self):
        return self.lhs is None and self.rhs is None

    """Metoda umozliwiajaca wyswietlenie zbudowanego drzewa."""

    def print_decision_tree(self):
        if self.lhs is None and self.rhs is None:
            print("LISC")
            print(self.indexes)
            print(self.node_class)
        else:
            print("-----------------------")
            print("PODDRZEWO")
            print(self.indexes)
            print("LEWO")
            self.lhs.print_decision_tree()
            print("PRAWO")
            self.rhs.print_decision_tree()
            print("-----------------------")
