# random_forest
### Uruchomienie

Aby uruchomic program należy:
    
    1. Wprowadzić pożądane parametry oraz scieżkę do pliku z danymi w main.py 
    2. Zapisać plik
    3. Uruchomić plik main.py 
    
Aby uruchomić prezentację działania algorytmu dla małego zbioru należy:
    
    1. Uruchomić plik test.py 

### Opis
https://gitlab.com/karkon/random_forest/-/wikis/Przewidywanie-spo%C5%BCycia-alkoholu-przez-student%C3%B3w 