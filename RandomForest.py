__author__ = "Michal Trendak & Karolina Kondzielnik"

import random
import statistics as stat

import numpy as np

from DecisionTree import DecisionTree

"""Klasa reprezentująca obiekt lasu losowego."""


class RandomForest:
    def __init__(self, values, classes, num_trees, num_attributes, num_rows):
        np.random.seed(123)
        """Ramka danych przykladow uczących"""
        self.values = values
        """Ramka danych niosaca informacje o przypisaniach przykladow do klas"""
        self.classes = classes
        """Liczba drzew, ktore maja zostac utworzone w ramach lasu losowego"""
        self.num_trees = num_trees
        """Liczba atrybutow wykorzystywanych do budowy drzew w ramach lasu losowego"""
        self.num_attributes = num_attributes
        """Liczba przykladow wykorzystywanych do budowy drzew w ramach lasu losowego"""
        self.num_rows = num_rows
        """Lista drzew zbudowanych w ramach lasu losowego"""
        self.nodes = [self.build_tree("DRZEWO " + str(i)) for i in range(num_trees)]

    def build_tree(self, name):
        print("loading...")
        # losowanie przykladow ze zwracaniem
        indexes = random.choices(np.array(range(len(self.classes))), k=self.num_rows)
        # losowanie atrybutow bez zwracania
        attr_indexes = np.sort(np.random.permutation(self.values.shape[1])[:self.num_attributes])
        new_tree = DecisionTree(self.values.iloc[indexes], self.classes.iloc[indexes], self.num_attributes,
                                attr_indexes, np.array(range(self.num_rows)), name)
        return new_tree

    """Metoda umozliwiajaca wykonanie klasyfikacji nowego przykladu."""

    def classify(self, values):
        return stat.mode([n.classify(values) for n in self.nodes])

    """Metoda umozliwiajaca wyswietlenie drzew zbudowanych w ramach lasu losowego."""

    def print_random_forest(self):
        for n in self.nodes:
            print("---------------DRZEWO---------------")
            n.print_decision_tree()
