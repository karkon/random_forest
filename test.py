__author__ = "Michal Trendak & Karolina Kondzielnik"

import pandas as pd

from DecisionTree import DecisionTree
from RandomForest import RandomForest

values = pd.DataFrame({'a': [1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0],
                       'b': [1, 1, 0, 1, 0, 0, 1, 1, 0, 1, 1, 1, 1, 1],
                       'c': [1, 0, 1, 1, 1, 1, 0, 0, 0, 0, 1, 0, 1, 1]})
classes = pd.DataFrame([1, 0, 1, 1, 1, 1, 1, 0, 0, 0, 1, 0, 1, 0])
tree = DecisionTree(values, classes, 3, [0, 1, 2], [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13])

print("----------------------------DECISION TREE----------------------------------")
tree.print_decision_tree()

print("------------------------------CLASSIFY-------------------------------------")
test = [1, 0, 1]
print("Wyznaczone: ", tree.classify(test), " Rzeczywiste: ", str(1))

print("----------------------------RANDOM FOREST----------------------------------")
random_f = RandomForest(values, classes, 3, 3, 7)

print("-------------------------RANDOM FOREST PRINT-------------------------------")
random_f.print_random_forest()

print("--------------------------RANDOM CLASSIFY----------------------------------")
print("Wyznaczone: ", random_f.classify(test), " Rzeczywiste: ", str(1))
