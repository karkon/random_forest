__author__      = "Michał Trendak & Karolina Kondzielnik"

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sn
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import confusion_matrix, accuracy_score
from sklearn.model_selection import train_test_split

from RandomForest import RandomForest

################################################################
#
# WCZYTANIE DANYCH I KONFIGURACJA PARAMETRÓW
#
################################################################
#
# Wczytujemy plik z danymi
dataset = pd.read_csv(r'D:\Projekty\random_forest\student-mat.csv')
#
# Liczba drzew B=2*abs(licznosc zbioru)
# liczność zbioru to 72
number_of_trees = 35
# Rozmiar zbioru testowegp
test_size = 0.25
# liczba atrybutów do każdego drzewa = licznosc zbioru / 10
number_of_attributes_in_every_tree = 7
# liczba przykładów do każego drzewa
number_of_rows_in_every_tree = 35

################################################################
#
# PRZYGOTOWANIE DANYCH
#
################################################################
#
# Usuwamy wiersze z pustymi wartościami
dataset = dataset.replace('?', np.nan)
dataset = dataset.dropna(axis=0, how="any")

# Wybieramy atrybut ktory bedzie klasa i wyciagamy wartosci dla przykladow
labels = np.array(dataset['Dalc'])

# Usuwamy wybrane atrybuty
dataset = dataset.drop(['age', 'G1', 'G2', 'Dalc'], axis=1)

# Zamiana atrybutu G3 na binarny z progiem 10.5
bin_G3 = [-1, 10.5, 20]
category = pd.cut(dataset.G3, bin_G3)
category = category.to_frame()
category.columns = ['code']
dataset["G3"] = category["code"].astype('category').cat.codes

bin_absences = [-1, 5.5, 100]
category = pd.cut(dataset.absences, bin_absences)
category = category.to_frame()
category.columns = ['code']
dataset["absences"] = category["code"].astype('category').cat.codes

bin_health = [-1, 3.5, 5]
category = pd.cut(dataset.health, bin_health)
category = category.to_frame()
category.columns = ['code']
dataset["health"] = category["code"].astype('category').cat.codes

bin_failures = [-1, 0.5, 5]
category = pd.cut(dataset.failures, bin_failures)
category = category.to_frame()
category.columns = ['code']
dataset["failures"] = category["code"].astype('category').cat.codes

# Zamieniamy kategoryczne słowne wartości binarne na liczbowe 0 1
dataset["sex"] = dataset["sex"].astype('category').cat.codes
dataset["school"] = dataset["school"].astype('category').cat.codes
dataset["address"] = dataset["address"].astype('category').cat.codes
dataset["famsize"] = dataset["famsize"].astype('category').cat.codes
dataset["Pstatus"] = dataset["Pstatus"].astype('category').cat.codes
dataset["schoolsup"] = dataset["schoolsup"].astype('category').cat.codes
dataset["famsup"] = dataset["famsup"].astype('category').cat.codes
dataset["paid"] = dataset["paid"].astype('category').cat.codes
dataset["activities"] = dataset["activities"].astype('category').cat.codes
dataset["nursery"] = dataset["nursery"].astype('category').cat.codes
dataset["higher"] = dataset["higher"].astype('category').cat.codes
dataset["romantic"] = dataset["romantic"].astype('category').cat.codes
dataset["internet"] = dataset["internet"].astype('category').cat.codes

# Rysujemy wykres korelacji zmiennych
corrMatrix = dataset.corr()
sn.heatmap(corrMatrix, cmap="YlGnBu")
plt.show()

# Zamieniamy wartości dyskretne na binarne
dataset = pd.get_dummies(dataset,
                         columns=["guardian", "Mjob", "Fjob", "Medu", "Fedu", "traveltime", "studytime", "reason",
                                  "famrel",
                                  "freetime", "goout", 'Walc'])

feature_list = list(dataset.columns)
dataset_SC = np.array(dataset)

# Podział na zbiór treningowy i testowy
train_features, test_features, train_labels, test_labels = train_test_split(dataset_SC, labels, test_size=0.25,
                                                                            random_state=42)
#################################################################
#
# RANDOM FOREST WŁASNA IMPLEMENTACJA
#
#################################################################

randomF = RandomForest(pd.DataFrame(train_features), pd.DataFrame(train_labels), number_of_trees,
                       number_of_attributes_in_every_tree,
                       number_of_rows_in_every_tree)
print("----TESTY-----")
own_random_tree_predictions = []
for i in test_features:
    own_random_tree_predictions.append(randomF.classify(i))
print("RANDOM FOREST IMPLEMENTACJA  WŁASNA")
# print(own_random_tree_predictions)
own_matrix = confusion_matrix(test_labels, own_random_tree_predictions)
print("confusion matric: \n", own_matrix)

own_accuracy = accuracy_score(test_labels, own_random_tree_predictions)
print("accuracy: ", own_accuracy)
################################################################
#
# RANDOM FOREST SCIKIT LEARN
#
################################################################

# Tworzymy klasyfikator
clf = RandomForestClassifier(n_estimators=number_of_trees, n_jobs=2, random_state=0)
clf.fit(train_features, train_labels)

# testujemy na zbiorze testowym
predictions = clf.predict(test_features)

# obliczamy macierz pomyłek i dokładność modelu
matrix = confusion_matrix(test_labels, predictions)
accuracy = accuracy_score(test_labels, predictions)
print("RANDOM FOREST SCIKIT LEARN")
print("confusion matric:")
print(matrix)
print("accuracy:", accuracy)
